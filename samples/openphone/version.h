/*
 * version.h
 *
 * Version number header file for OpenPhone
 *
 * A H.323 "net telephone" application.
 *
 * Copyright (c) 1993-1998 Equivalence Pty. Ltd.
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is Portable Windows Library.
 *
 * The Initial Developer of the Original Code is Equivalence Pty. Ltd.
 *
 * Portions of this code were written with the assisance of funding from
 * Vovida Networks, Inc. http://www.vovida.com.
 *
 * Portions are Copyright (C) 1993 Free Software Foundation, Inc.
 * All Rights Reserved.
 *
 * Contributor(s): ______________________________________.
 *
 *
 * Library dependencies:
 *
 *   pwlib: v1.3.5   CVS tag: v1_3_5
 *   opal: v2.0beta1   CVS tag: HEAD
 */

#ifndef _OpenPhone_VERSION_H
#define _OpenPhone_VERSION_H

#define MAJOR_VERSION 2
#define MINOR_VERSION 0
#define BUILD_TYPE    AlphaCode
#define BUILD_NUMBER 0


#endif  // _OpenPhone_VERSION_H


// End of File ///////////////////////////////////////////////////////////////
