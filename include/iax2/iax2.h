/*
 *
 * Inter Asterisk Exchange 2
 * 
 * Base include file of iax.
 *
 * Open Phone Abstraction Library (OPAL)
 *
 * Copyright (c) 2005 Indranet Technologies Ltd.
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is Open Phone Abstraction Library.
 *
 * The Initial Developer of the Original Code is Indranet Technologies Ltd.
 *
 * The author of this code is Derek J Smithies
 *
 *
 * Contributor(s): ______________________________________.
 *
 * $Log$
 * Revision 1.1  2005/07/30 07:01:32  csoutheren
 * Added implementation of IAX2 (Inter Asterisk Exchange 2) protocol
 * Thanks to Derek Smithies of Indranet Technologies Ltd. for
 * writing and contributing this code
 *
 *
 *
 */

#ifndef __OPAL_IAX_H
#define __OPAL_IAX_H

#include <iax2/iax2ep.h>
#include <iax2/iax2con.h>


#endif // __OPAL_IAX_H


// End of File ///////////////////////////////////////////////////////////////
