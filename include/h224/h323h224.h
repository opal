/*
 * h323h224.h
 *
 * H.323 H.224 logical channel establishment implementation for the 
 * OpenH323 Project.
 *
 * Copyright (c) 2006 Network for Educational Technology, ETH Zurich.
 * Written by Hannes Friederich.
 *
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and limitations
 * under the License.
 *
 * Contributor(s): ______________________________________.
 *
 * $Log$
 * Revision 1.5  2007/08/05 22:34:08  hfriederich
 * Merge from MediaTypeBranch.
 *
 * New features:
 * - Session ID replaced by OpalMediaType class
 * - Dynamic session ID assignment in H.323 according to H.245
 * - Attempt to dynamically handle different media types (audio / video /
 *    / fax / H.224 etc) instead of hardcoded in the core classes.
 *   (not completed yet)
 * - Improved propagation of media formats / options between connections
 * - New media command implementation, uses chain-like propagation
 * - SDP 'Capabilities' to ease handling of complex SDP parameters
 *   (like FMTP)
 * - New SIP Transaction handling, avoiding race conditions
 * - Re-written H.224 implementation as an example how to add additional
 *   media types without adding code to the base classes
 *   (connection / endpoint / manager etc)
 *
 * This commit breaks the following code:
 * - T38Fax implementation.
 * - Recently added FMTP handling in MediaFormat/Option and SDP.
 *
 * Revision 1.4  2007/04/19 06:17:20  csoutheren
 * Fixes for precompiled headers with gcc
 *
 * Revision 1.3  2006/08/10 05:10:30  csoutheren
 * Various H.323 stability patches merged in from DeimosPrePLuginBranch
 *
 * Revision 1.2.2.1  2006/08/09 12:49:20  csoutheren
 * Improve stablity under heavy H.323 load
 *
 * Revision 1.2  2006/04/30 09:25:08  csoutheren
 * Remove warning about missing newline
 *
 * Revision 1.1  2006/04/20 16:48:17  hfriederich
 * Initial version of H.224/H.281 implementation.
 *
 */

#ifndef __OPAL_H323H224_H
#define __OPAL_H323H224_H

#ifdef P_USE_PRAGMA
#pragma interface
#endif

#ifndef _PTLIB_H
#include <ptlib.h>
#endif

#include <h323/h323caps.h>

#include <h224/h224mediafmt.h>
#include <h224/h224.h>
#include <h224/h224handler.h>

#define OPAL_H224_CAPABILITY_NAME "H.224"

/** This class describes the H.224 capability
*/
class H323_H224Capability : public H323DataCapability
{
  PCLASSINFO(H323_H224Capability, H323DataCapability);
  
public:
  
  H323_H224Capability();
  ~H323_H224Capability();
  
  Comparison Compare(const PObject & obj) const;
  
  virtual PObject * Clone() const;
  
  virtual unsigned GetSubType() const;
  
  virtual PString GetFormatName() const;
  
  virtual H323Channel * CreateChannel(H323Connection & connection,
									  H323Channel::Directions dir,
									  unsigned sesionID,
									  const H245_H2250LogicalChannelParameters * param) const;
  
  virtual BOOL OnSendingPDU(H245_DataApplicationCapability & pdu) const;
  virtual BOOL OnSendingPDU(H245_DataMode & pdu) const;
  virtual BOOL OnReceivedPDU(const H245_DataApplicationCapability & pdu);
  
};

/** This class implements a H.224 logical channel
*/
class H323_H224Channel : public H323Channel
{
  PCLASSINFO(H323_H224Channel, H323Channel);
  
public:
  H323_H224Channel(H323Connection & connection,
				   const H323Capability & capability,
				   Directions direction,
				   RTP_UDP & session,
				   unsigned sessionID);
  ~H323_H224Channel();
  
  virtual H323Channel::Directions GetDirection() const;
  virtual BOOL SetInitialBandwidth();
		
  virtual BOOL Open();
  virtual BOOL Start();
  virtual void Close();
  
  virtual BOOL OnSendingPDU(H245_OpenLogicalChannel & openPDU) const;
  virtual void OnSendOpenAck(const H245_OpenLogicalChannel & openPDU, 
							 H245_OpenLogicalChannelAck & ack) const;
  virtual BOOL OnReceivedPDU(const H245_OpenLogicalChannel & pdu, unsigned & errorCode);
  virtual BOOL OnReceivedAckPDU(const H245_OpenLogicalChannelAck & pdu);
  
  virtual BOOL OnSendingPDU(H245_H2250LogicalChannelParameters & param) const;
  virtual void OnSendOpenAck(H245_H2250LogicalChannelAckParameters & param) const;
  virtual BOOL OnReceivedPDU(const H245_H2250LogicalChannelParameters & param,
							 unsigned & errorCode);
  virtual BOOL OnReceivedAckPDU(const H245_H2250LogicalChannelAckParameters & param);
  
  virtual BOOL SetDynamicRTPPayloadType(int newType);
  RTP_DataFrame::PayloadTypes GetDynamicRTPPayloadType() const { return rtpPayloadType; }
  
  virtual OpalMediaStream * GetMediaStream(BOOL deleted = FALSE) const;
  
  OpalH224Handler * GetHandler() const { return h224Handler; }
  
protected:
	
	virtual BOOL ExtractTransport(const H245_TransportAddress & pdu,
								  BOOL isDataPort,
								  unsigned & errorCode);
  
  unsigned sessionID;
  Directions direction;
  RTP_UDP & rtpSession;
  H323_RTP_Session & rtpCallbacks;
  OpalH224Handler *h224Handler;
  RTP_DataFrame::PayloadTypes rtpPayloadType;
  
};

#define OPAL_REGISTER_H224_CAPABILITY() \
static H323CapabilityFactory::Worker<H323_H224Capability> h224Factory(OpalH224, true);

#endif // __OPAL_H323H224_H

